import numpy as np
import fasttext
import os

def l2Normalize(kmer):
	newKmer = kmer / np.linalg.norm(kmer)
	return newKmer


def calculateJ(dict1, dict2, kmers, exponential):
    firstList = np.zeros((1, 5000))
    secondList = np.zeros((1, 5000))


    for x in range(0, 5000):
        for key in dict1.keys():
            if kmers[x] == key:
                firstList[0, x] = 1.0

    for y in range(0, 5000):
        for key in dict2.keys():
            if kmers[y] == key:
                secondList[0, y] = 1.0


    minSum = 0.0
    maxSum = 0.0
    jaccardScore = 0.0
    for i in range(0, 5000):
        for j in range(0, 5000): 
            if firstList[0,i] == 0 and secondList[0,j] == 0:
            	continue
            else:
            	kmerWeight = np.dot(l2Normalize(model[kmers[i]]), l2Normalize(model[kmers[j]]))
            	if kmerWeight < 0.0:
            		kmerWeight = 0.0
            	weight = kmerWeight**exponential
            	minSum = minSum + weight * min(firstList[0, i], secondList[0, j])
            	maxSum = maxSum + weight * max(firstList[0, i], secondList[0, j])
        if i == 4999 and j == 4999:
            kmerWeight = np.dot(l2Normalize(model[kmers[i]]), l2Normalize(model[kmers[j]])) 
            if kmerWeight < 0.0:
            	kmerWeight = 0.0
            weight = kmerWeight**exponential
            minSum = minSum + weight * min(firstList[0, i], secondList[0, j])
            maxSum = maxSum + weight * max(firstList[0, i], secondList[0, j])
    jaccardScore = minSum / maxSum

    k = 15
    eps = 0.00000001
    print(jaccardScore)
    DValue = (-1.0 / k) * np.log((2.0 * jaccardScore) / (1 + jaccardScore + eps))
    return DValue


model = fasttext.load_model('15GramGENOME.bin')

with open('') as f:
    contentOne = f.readlines()

with open('') as g:
    contentTwo = g.readlines()
#text files from genometester that had the kmers from organism1 and organism2

firstSeq = []
secondSeq = []

#from genometester and sampling 5000 kmers from the union of kmers of organism1 and organism2, .npy file
kmers = np.load('')

kmers = sorted(kmers)

for x in range(0, len(contentOne) - 2): #minus two because of how the txt file is composed
    firstS = contentOne[x].split('\t')[0]
    firstSeq.append(firstS)

for y in range(0, len(contentTwo) - 2):
    secondS = contentTwo[y].split('\t')[0]
    secondSeq.append(secondS)

kDict = {}
for iterations in range(0, len(firstSeq)):
    if any(km in firstSeq[iterations] for km in kmers):
        if kDict.has_key(firstSeq[iterations]):
            kDict[firstSeq[iterations]] = 1 #making sure to make it one for the dictionary item
        else: #needed this to make sure that the dictionary would have 1 for that ecoli
            kDict[firstSeq[iterations]] = 1

kDict1 = {}
for iterations in range(0, len(secondSeq)):
    if any(km in secondSeq[iterations] for km in kmers):
        if kDict1.has_key(secondSeq[iterations]):
            kDict1[secondSeq[iterations]] = 1
        else: ##needed this to make sure that the dictionary would have 1 for that ecoli
            kDict1[secondSeq[iterations]] = 1

distance = calculateJ(kDict, kDict1, kmers, 8)